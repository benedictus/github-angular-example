
import {RouterModule} from '@angular/router';
import {GithubUserViewComponent} from './github/component/github-user-view/github-user-view.component';



export const routing = RouterModule.forRoot([
  {path: '', component: GithubUserViewComponent}
]);
