import {Component, Input, OnInit} from '@angular/core';
import {GithubUser} from '../../service/data/github-user';

@Component({
  selector: 'app-github-follower',
  templateUrl: './github-follower.component.html',
  styleUrls: ['./github-follower.component.scss']
})
export class GithubFollowerComponent implements OnInit {

  constructor() {
  }

  @Input('user') user: GithubUser;

  ngOnInit() {
  }

}
