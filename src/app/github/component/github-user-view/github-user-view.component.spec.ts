import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GithubUserViewComponent } from './github-user-view.component';

describe('GithubUserViewComponent', () => {
  let component: GithubUserViewComponent;
  let fixture: ComponentFixture<GithubUserViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GithubUserViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GithubUserViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
