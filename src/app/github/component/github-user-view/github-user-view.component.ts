import {Component, OnDestroy, OnInit} from '@angular/core';
import {GithubUserService} from '../../service/github/github-user.service';
import {GithubUser} from '../../service/data/github-user';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-github-user-view',
  templateUrl: './github-user-view.component.html',
  styleUrls: ['./github-user-view.component.scss']
})
export class GithubUserViewComponent implements OnInit {

  private queryParamsSubscription: Subscription;

  userNamePlaceholder: string;
  userName: string;
  findUserError = false;
  findFollowersError = false;

  user: GithubUser;
  followers: GithubUser[];

  constructor(private service: GithubUserService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
    this.queryParamsSubscription = this.route.queryParams.subscribe(params => {
      this.userName = params['name'];
      this.userNamePlaceholder = params['name'];
    });
    if (!this.userName) {
      this.userName = '';
    }
  }

  findUser() {
    this.findUserError = false;
    this.service.getUser(this.userName).then(
      user => {
        this.user = user;
        this.findFollowers();
      }).catch(
      e => {
        this.findUserError = true;
      });
  }

  findFollowers() {
    this.findFollowersError = false;
    this.service.getFollowers(this.userName).then(
      followers => {
        this.followers = followers;
      }).catch(
      e => {
        this.findFollowersError = true;
      });
  }

  onUserNameChange() {
    this.userName = this.userNamePlaceholder;
    this.findUserError = false;
    this.findFollowersError = false;
    this.router.navigate(['/'], {queryParams: {name: this.userName}});
  }

}
