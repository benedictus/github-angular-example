import {Component, Input, OnInit} from '@angular/core';
import {GithubUser} from '../../service/data/github-user';

@Component({
  selector: 'app-github-user',
  templateUrl: './github-user.component.html',
  styleUrls: ['./github-user.component.scss']
})
export class GithubUserComponent implements OnInit {


  @Input('user') user: GithubUser;

  constructor() { }

  ngOnInit() {
  }

}
