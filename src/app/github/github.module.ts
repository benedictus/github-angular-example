import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GithubUserService} from './service/github/github-user.service';
import {GithubUserComponent} from './component/github-user/github-user.component';
import {GithubUserViewComponent} from './component/github-user-view/github-user-view.component';
import {SharedModule} from '../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import { GithubFollowerComponent } from './component/github-follower/github-follower.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [
    GithubUserViewComponent,
    GithubUserComponent,
    GithubFollowerComponent
  ],
  exports: [
    GithubUserViewComponent
  ],
  providers: [
    GithubUserService
  ]
})
export class GithubModule {
}
