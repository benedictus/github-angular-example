import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {GithubUser} from '../data/github-user';
import {WebClientService} from '../../../shared/service/web-client.service';
import {HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable()
export class GithubUserService {

  private readonly githubApiUrl = '/github';
  private readonly usersEndpoint = '/users';
  private readonly followersEndpoint = '/followers';
  private readonly api: string;

  constructor(private client: WebClientService) {
    this.api = environment.githubApi + this.usersEndpoint;
  }

  getUser(userName: string): Promise<GithubUser> {
    const url = `${this.api}/${userName}`;
    return this.client.get(url).toPromise().then(
      response => response as GithubUser
    );
  }

  getFollowers(userName: string, page?: number, size?: number): Promise<GithubUser[]> {
    const url = `${this.api}/${userName}${this.followersEndpoint}`;
    const params = new HttpParams();
    if (page && page > 0) {
      params.set('page', page.toString());
    }
    if (size && size > 0) {
      params.set('per_page', size.toString());
    }
    return this.client.get(url, params).toPromise().then(
      response => response as GithubUser[]
    );
  }


}
