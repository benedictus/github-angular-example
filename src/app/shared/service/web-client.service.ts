import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class WebClientService {

  constructor(private http: HttpClient) {
  }


  post(url, data) {
    return this.http.post(url, data);
  }

  get(url, params?: HttpParams) {
    return this.http.get(url, {params: params});
  }

  delete(url) {
    return this.http.delete(url);
  }

  put(url, data) {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    const options = {headers: headers};
    return this.http.put(url, data, options);
  }


}
