import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WebClientService} from './service/web-client.service';
import {HttpClient, HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    WebClientService,
    HttpClient
  ]
})
export class SharedModule { }
