export const environment = {
  production: true,
  defaultLocale: 'en',
  githubApi: 'https://api.github.com'
};
